<?php

namespace Tests\Feature\Articles;

use App\Models\Article;
use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateArticlesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function can_create_articles()
    {
        $user = User::factory()->create();

        $category = Category::factory()->create();

        $response =$this->postJson(route('api.v1.articles.store'),[
           'title' => 'Nuevo articulo',
           'slug' => 'nuevo-articulo',
           'content' => 'Contenido del articulo',
            '_relationships' => [
                'category' => $category,
                'author' => $user
            ]
        ])->assertCreated();

        $article = Article::first();


        $response->assertJsonApiResource($article,[
            'title' => 'Nuevo articulo',
            'slug' => 'nuevo-articulo',
            'content' => 'Contenido del articulo'
        ]);

        $this->assertDatabaseHas('articles',[
            'title' => 'Nuevo articulo',
            'user_id' => $user->id,
            'category_id' => $category->id,
        ]);
    }

    /** @test */
    public function title_is_required()
    {
        $this->postJson(route('api.v1.articles.store'),[
            'slug' => 'nuevo-articulo',
            'content' => 'Contenido del articulo'
        ])->assertJsonApiValidationErrors('title');
    }

    /** @test */
    public function title_must_be_at_least_4_characters()
    {
        $this->postJson(route('api.v1.articles.store'),[
            'title' => 'nue',
            'slug' => 'nuevo-articulo',
            'content' => 'Contenido del articulo'
        ])->assertJsonApiValidationErrors('title');

    }

    /** @test */
    public function slug_is_required()
    {
        $this->postJson(route('api.v1.articles.store'),[
            'title' => 'Nuevo articulo',
            'content' => 'Contenido del articulo'
        ])->assertJsonApiValidationErrors('slug');

    }

    /** @test */
    public function slug_must_be_unique()
    {
        $article = Article::factory()->create();

        $this->postJson(route('api.v1.articles.store'),[
            'title' => 'Nuevo articulo',
            'slug' => $article->slug,
            'content' => 'Contenido del articulo'
        ])->assertJsonApiValidationErrors('slug');

    }

    /** @test */
    public function slug_must_only_contain_letters_numbers_and_dashes()
    {
        $this->postJson(route('api.v1.articles.store'),[
            'title' => 'Nuevo articulo',
            'slug' => '$%^#',
            'content' => 'Contenido del articulo'
        ])->assertJsonApiValidationErrors('slug');
    }

    /** @test */
    public function slug_must_not_contain_underscores()
    {
        $this->postJson(route('api.v1.articles.store'),[
            'title' => 'Nuevo articulo',
            'slug' => 'with_underscores',
            'content' => 'Contenido del articulo'
        ])->assertSee(trans(
            'validation.no_underscores', [
                'attribute'=>'data.attributes.slug'
            ]))
            ->assertJsonApiValidationErrors('slug');
    }

    /** @test */
    public function slug_must_not_start_with_dashes()
    {
        $this->postJson(route('api.v1.articles.store'),[
            'title' => 'Nuevo articulo',
            'slug' => '-start-with-dashes',
            'content' => 'Contenido del articulo'
        ])->assertSee(__(
            'validation.no_starting_dashes', [
            'attribute'=>'data.attributes.slug'
        ]))->assertJsonApiValidationErrors('slug');

    }

    /** @test */
    public function slug_must_not_end_with_dashes()
    {
        $this->postJson(route('api.v1.articles.store'),[
            'title' => 'Nuevo articulo',
            'slug' => 'end-with-dashes-',
            'content' => 'Contenido del articulo'
        ])->assertSee(__(
            'validation.no_ending_dashes', [
            'attribute'=>'data.attributes.slug'
        ]))->assertJsonApiValidationErrors('slug');
    }

    /** @test */
    public function content_is_required()
    {
        $this->postJson(route('api.v1.articles.store'),[
            'title' => 'Nuevo articulo',
            'slug' => 'nuevo-articulo'
          ])->assertJsonApiValidationErrors('content');

    }

    /** @test */
    public function category_relationship_is_required()
    {
        $this->postJson(route('api.v1.articles.store'),[
            'title' => 'Nuevo articulo',
            'slug' => 'nuevo-articulo',
            'content'=> 'Contenido del Articulo',
          ])->assertJsonApiValidationErrors('relationships.category');

    }

    /** @test */
    public function category_must_exists_in_database()
    {
        $this->postJson(route('api.v1.articles.store'),[
            'title' => 'Nuevo articulo',
            'slug' => 'nuevo-articulo',
            'content'=> 'Contenido del Articulo',
            '_relationships'=>[
                'category' => Category::factory()->make()
            ]
          ])->assertJsonApiValidationErrors('relationships.category');

    }
}
